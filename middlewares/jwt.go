package middlewares

import (
	"golang_api_starter_pack/config"
	"golang_api_starter_pack/models"

	"github.com/labstack/echo/middleware"
)

var JWTConfig = middleware.JWTConfig{
	Claims:     &models.JwtCustomClaims{},
	SigningKey: []byte(config.Configuration().API.Secret),
}
