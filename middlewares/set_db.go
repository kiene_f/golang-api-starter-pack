package middlewares

import (
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
)

type Database struct {
	DB *gorm.DB
}

func (db *Database) SetDBtoContext(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		c.Set("DB", db.DB)
		if err := next(c); err != nil {
			c.Error(err)
		}
		return nil
	}
}
