package server

import (
	"golang_api_starter_pack/config"
	"golang_api_starter_pack/middlewares"
	"golang_api_starter_pack/router"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

// Setup the web erver
func Setup(db *gorm.DB) *echo.Echo {
	e := echo.New()
	c := config.Configuration()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{c.API.AllowOrigins},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))
	d := &middlewares.Database{DB: db}
	e.Use(d.SetDBtoContext)
	router.Initialize(e)
	return e
}
