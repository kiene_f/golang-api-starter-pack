package config

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/spf13/viper"
)

type Config struct {
	DB struct {
		Dbname  string `mapstructure:"dbname"`
		Schema  string `mapstructure:"schema"`
		Host    string `mapstructure:"host"`
		Port    string `mapstructure:"port"`
		User    string `mapstructure:"user"`
		Pass    string `mapstructure:"pass"`
		Sslmode string `mapstructure:"sslmode"`
	} `mapstructure:"postgres"`
	API struct {
		AllowOrigins string `mapstructure:"alloworigins"`
		Port         string `mapstructure:"port"`
		Secret       string `mapstructure:"secret"`
	} `mapstructure:"api"`
}

var c Config

func Configuration() *Config {
	appPath, err := GetAppDirPath()
	if err != nil {
		log.Fatal(4, "Failed to get app path: %v", err)
	}

	_, err = os.Stat(appPath + "/config/config.toml")
	if err != nil {
		log.Fatal(4, "Failed to get config file: %v", err)
	}
	if !os.IsNotExist(err) && c.API.Secret == "" {
		v := viper.New()
		v.SetConfigName("config")
		v.AddConfigPath(appPath + "/config/")
		if err := v.ReadInConfig(); err != nil {
			fmt.Printf("couldn't load config: %s", err)
			return nil
		}
		if err := v.Unmarshal(&c); err != nil {
			fmt.Printf("couldn't read config: %s", err)
			return nil
		}
	}
	return &c
}

func GetAppDirPath() (string, error) {
	IsWindows := runtime.GOOS == "windows"
	var appPath string
	var err error
	if IsWindows && filepath.IsAbs(os.Args[0]) {
		appPath = filepath.Clean(os.Args[0])
	} else {
		appPath, err = exec.LookPath(os.Args[0])
	}

	if err != nil {
		return "", err
	}
	appPath, err = filepath.Abs(filepath.Dir(appPath))
	if err != nil {
		return "", err
	}
	// Note: we don't use path.Dir here because it does not handle case
	//	which path starts with two "/" in Windows: "//psf/Home/..."
	return strings.Replace(appPath, "\\", "/", -1), err
}
