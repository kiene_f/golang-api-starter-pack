package database

import (
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
)

func DBInstance(c echo.Context) *gorm.DB {
	return c.Get("DB").(*gorm.DB)
}
