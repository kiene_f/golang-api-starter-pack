package main

import (
	"golang_api_starter_pack/cmd"
)

func main() {
	cmd.Execute()
}
