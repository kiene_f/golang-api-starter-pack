package cmd

import (
	"github.com/spf13/cobra"
)

// rootCmd represents the root command
var rootCmd = &cobra.Command{
	Use:   "my golang api",
	Short: "my golang api is a golang api",
	Long:  `my golang api is a golang api ...`,
}

func Execute() {
	rootCmd.Execute()
}
