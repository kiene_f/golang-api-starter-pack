package cmd

import (
	"fmt"
	"golang_api_starter_pack/config"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/golang-migrate/migrate"
	"github.com/spf13/cobra"
)

var migrateUpCmd = &cobra.Command{
	Use:   "up [n]",
	Short: "Apply all or [n] up migrations",
	Long:  `Apply all or [n] up migrations`,
	Run: func(cmd *cobra.Command, args []string) {
		// Get configuration
		c := config.Configuration()
		// DB Connection
		psqlInfo := fmt.Sprintf("postgresql://%s:%s@%s:%v/%s?sslmode=%s&search_path=%s",
			c.DB.User, c.DB.Pass, c.DB.Host, c.DB.Port, c.DB.Dbname, c.DB.Sslmode, c.DB.Schema)
		// Create migration
		appPath, err := config.GetAppDirPath()
		if err != nil {
			log.Fatal(4, "Failed to get app path: %v", err)
		}
		m, err := migrate.New("file://"+appPath+"/migrations", psqlInfo)
		if err != nil {
			fmt.Println(err)
			return
		}
		// Migrate
		if len(args) == 1 {
			limit, err := strconv.Atoi(args[0])
			if err != nil {
				fmt.Println(err)
				return
			}
			if err := m.Steps(limit); err != nil {
				if err != migrate.ErrNoChange {
					log.Fatal(err)
				} else {
					fmt.Println(err)
				}
			}
		} else {
			if err := m.Up(); err != nil {
				if err != migrate.ErrNoChange {
					log.Fatal(err)
				} else {
					fmt.Println(err)
				}
			}
		}
	},
}

var migrateDownCmd = &cobra.Command{
	Use:   "down [n] or [all]",
	Short: "Apply [all] or [n] down migrations",
	Long:  `Apply [all] or [n] down migrations`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		// Get configuration
		c := config.Configuration()
		// DB Connection
		psqlInfo := fmt.Sprintf("postgresql://%s:%s@%s:%v/%s?sslmode=%s&search_path=%s",
			c.DB.User, c.DB.Pass, c.DB.Host, c.DB.Port, c.DB.Dbname, c.DB.Sslmode, c.DB.Schema)
		// Create migration
		appPath, err := config.GetAppDirPath()
		if err != nil {
			log.Fatal(4, "Failed to get app path: %v", err)
		}
		m, err := migrate.New("file://"+appPath+"/migrations", psqlInfo)
		if err != nil {
			fmt.Println(err)
			return
		}
		// Migrate
		if args[0] == "all" {
			if err := m.Down(); err != nil {
				if err != migrate.ErrNoChange {
					log.Fatal(err)
				} else {
					fmt.Println(err)
				}
			}
		} else {
			limit, err := strconv.Atoi(args[0])
			if err != nil {
				fmt.Println(err)
				return
			}
			if err := m.Steps(-limit); err != nil {
				if err != migrate.ErrNoChange {
					log.Fatal(err)
				} else {
					fmt.Println(err)
				}
			}
		}
	},
}

func createFile(fname string) {
	if _, err := os.Create(fname); err != nil {
		log.Fatal(err)
	}
}

var migrateCreateCmd = &cobra.Command{
	Use:   "create [name]",
	Short: "Create a set of timestamped up/down migrations titled [name]",
	Long:  `Create a set of timestamped up/down migrations titled [name]`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		appPath, err := config.GetAppDirPath()
		if err != nil {
			log.Fatal(4, "Failed to get app path: %v", err)
		}
		name := args[0]
		dir := appPath + "/migrations/"
		timestamp := time.Now().Unix()
		base := fmt.Sprintf("%v%v_%v.", dir, timestamp, name)
		os.MkdirAll(dir, os.ModePerm)
		createFile(base + "up.sql")
		createFile(base + "down.sql")
	},
}

var migrateGoToCmd = &cobra.Command{
	Use:   "goto [v]",
	Short: "Migrate to version [v]",
	Long:  `Migrate to version [v]`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {

		v, err := strconv.ParseUint(args[0], 10, 64)
		if err != nil {
			log.Fatal("error: can't read version argument v")
		}

		// Get configuration
		c := config.Configuration()
		// DB Connection
		psqlInfo := fmt.Sprintf("postgresql://%s:%s@%s:%v/%s?sslmode=%s&search_path=%s",
			c.DB.User, c.DB.Pass, c.DB.Host, c.DB.Port, c.DB.Dbname, c.DB.Sslmode, c.DB.Schema)
		// Create migration
		appPath, err := config.GetAppDirPath()
		if err != nil {
			log.Fatal(4, "Failed to get app path: %v", err)
		}
		m, err := migrate.New("file://"+appPath+"/migrations", psqlInfo)
		if err != nil {
			fmt.Println(err)
			return
		}

		if err := m.Migrate(uint(v)); err != nil {
			if err != migrate.ErrNoChange {
				log.Fatal(err)
			} else {
				log.Fatal(err)
			}
		}
	},
}

var migrateForceCmd = &cobra.Command{
	Use:   "force [v]",
	Short: "Set version [v] but don't run migration (ignores dirty state)",
	Long:  `Set version [v] but don't run migration (ignores dirty state)`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {

		v, err := strconv.Atoi(args[0])
		if err != nil {
			log.Fatal("error: can't read version argument v")
		}

		// Get configuration
		c := config.Configuration()
		// DB Connection
		psqlInfo := fmt.Sprintf("postgresql://%s:%s@%s:%v/%s?sslmode=%s&search_path=%s",
			c.DB.User, c.DB.Pass, c.DB.Host, c.DB.Port, c.DB.Dbname, c.DB.Sslmode, c.DB.Schema)
		// Create migration
		appPath, err := config.GetAppDirPath()
		if err != nil {
			log.Fatal(4, "Failed to get app path: %v", err)
		}
		m, err := migrate.New("file://"+appPath+"/migrations", psqlInfo)
		if err != nil {
			fmt.Println(err)
			return
		}

		if err := m.Force(v); err != nil {
			log.Fatal(err)
		}
	},
}

var migrateVersionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print current migration version",
	Long:  `Print current migration version`,
	Run: func(cmd *cobra.Command, args []string) {
		// Get configuration
		c := config.Configuration()
		// DB Connection
		psqlInfo := fmt.Sprintf("postgresql://%s:%s@%s:%v/%s?sslmode=%s&search_path=%s",
			c.DB.User, c.DB.Pass, c.DB.Host, c.DB.Port, c.DB.Dbname, c.DB.Sslmode, c.DB.Schema)
		// Create migration
		appPath, err := config.GetAppDirPath()
		if err != nil {
			log.Fatal(4, "Failed to get app path: %v", err)
		}
		m, err := migrate.New("file://"+appPath+"/migrations", psqlInfo)
		if err != nil {
			fmt.Println(err)
			return
		}
		// Get version
		v, dirty, err := m.Version()
		if err != nil {
			log.Fatal(err)
		}
		if dirty {
			fmt.Printf("%v (dirty)\n", v)
		} else {
			fmt.Println(v)
		}
	},
}

var migrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "migration",
	Long:  `migration`,
}

func init() {
	rootCmd.AddCommand(migrateCmd)
	migrateCmd.AddCommand(migrateUpCmd)
	migrateCmd.AddCommand(migrateDownCmd)
	migrateCmd.AddCommand(migrateVersionCmd)
	migrateCmd.AddCommand(migrateCreateCmd)
	migrateCmd.AddCommand(migrateGoToCmd)
	migrateCmd.AddCommand(migrateForceCmd)
}
