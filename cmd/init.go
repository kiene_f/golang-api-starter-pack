package cmd

import (
	"fmt"
	"io"
	"log"
	"os"

	"github.com/spf13/cobra"
)

// Copy the src file to dst. Any existing file will be overwritten and will not
// copy file attributes.
func Copy(src, dst string) error {
	in, err := os.Open(src)
	if err != nil {
		return err
	}
	defer in.Close()

	out, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, in)
	if err != nil {
		return err
	}
	return out.Close()
}

// initCmd represents the init command
var initCmd = &cobra.Command{
	Use:   "init",
	Short: "Create the config file",
	Long:  `Create the config file`,
	Run: func(cmd *cobra.Command, args []string) {
		path := "./config/"
		dest := path + "config.toml"
		src := path + "config.example.toml"
		if _, err := os.Stat(dest); os.IsNotExist(err) {
			if err := Copy(src, dest); err != nil {
				log.Fatal(err)
			}
		}
		fmt.Println("init done.")
	},
}

func init() {
	rootCmd.AddCommand(initCmd)
}
