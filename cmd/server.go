package cmd

import (
	"golang_api_starter_pack/config"
	"golang_api_starter_pack/server"
	"fmt"
	"os"
	"time"

	"github.com/tylerb/graceful"

	"github.com/jinzhu/gorm"
	"github.com/qor/validations"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// serverCmd represents the server command
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "Start the server (API)",
	Long:  `The server command start the server (API)`,
	Run: func(cmd *cobra.Command, args []string) {
		c := config.Configuration()
		// Set secret in env
		viper.SetEnvPrefix("jwt")
		viper.BindEnv("secret")
		os.Setenv("JWT_SECRET", c.API.Secret)

		psqlInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
			c.DB.Host, c.DB.Port, c.DB.User, c.DB.Pass, c.DB.Dbname)
		db, err := gorm.Open("postgres", psqlInfo)
		db.LogMode(true)
		if err != nil {
			fmt.Println(err)
			return
		}
		s := server.Setup(db)
		validations.RegisterCallbacks(db)

		s.Server.Addr = ":" + c.API.Port
		// Graceful Shutdown
		graceful.ListenAndServe(s.Server, 5*time.Second)
	},
}

func init() {
	rootCmd.AddCommand(serverCmd)
}
