package cmd

import (
	"database/sql"
	"fmt"
	"golang_api_starter_pack/config"
	"io"
	"io/ioutil"
	"log"
	"os"

	_ "github.com/golang-migrate/migrate/database/postgres"
	_ "github.com/golang-migrate/migrate/source/file"
	"github.com/spf13/cobra"
)

var bootstrapResetCmd = &cobra.Command{
	Use:   "reset",
	Short: "Reset the database",
	Long:  `Reset the database`,
	Run: func(cmd *cobra.Command, args []string) {
		// Get configuration
		c := config.Configuration()
		// DB Connection
		psqlInfo := fmt.Sprintf("postgresql://%s:%s@%s:%v/%s?sslmode=%s&search_path=%s",
			c.DB.User, c.DB.Pass, c.DB.Host, c.DB.Port, c.DB.Dbname, c.DB.Sslmode, c.DB.Schema)
		db, err := sql.Open("postgres", psqlInfo)
		if err != nil {
			panic(err)
		}
		defer db.Close()
		err = db.Ping()
		if err != nil {
			panic(err)
		}
		// Drop default schema and sync
		_, err = db.Exec("DROP SCHEMA IF EXISTS sync, " + c.DB.Schema + " CASCADE")
		if err != nil {
			panic(err)
		}
		// Create default schema
		_, err = db.Exec("CREATE SCHEMA IF NOT EXISTS " + c.DB.Schema)
		if err != nil {
			panic(err)
		}
	},
}

func IsEmpty(name string) (bool, error) {
	f, err := os.Open(name)
	if err != nil {
		return false, err
	}
	defer f.Close()

	names, err := f.Readdirnames(0) // Or f.Readdir(1)
	if err == io.EOF {
		return true, nil
	}

	if len(names) == 1 && names[0] == ".gitkeep" {
		return true, nil
	}
	return false, err // Either not empty or error, suits both cases
}

var bootstrapCmd = &cobra.Command{
	Use:   "bootstrap",
	Short: "Bootstrap the project (create table, migration & trigger)",
	Long: `The bootstrap command creates the database by: creating the different tables, 
	doing migrations and adding tiggers`,
	Run: func(cmd *cobra.Command, args []string) {
		// Get configuration
		c := config.Configuration()
		// DB Connection
		psqlInfo := fmt.Sprintf("postgresql://%s:%s@%s:%v/%s?sslmode=%s&search_path=%s",
			c.DB.User, c.DB.Pass, c.DB.Host, c.DB.Port, c.DB.Dbname, c.DB.Sslmode, c.DB.Schema)
		db, err := sql.Open("postgres", psqlInfo)
		if err != nil {
			panic(err)
		}
		defer db.Close()
		err = db.Ping()
		if err != nil {
			panic(err)
		}
		// Construct DB
		appPath, err := config.GetAppDirPath()
		if err != nil {
			log.Fatal(4, "Failed to get app path: %v", err)
		}
		file, err := ioutil.ReadFile(appPath + "/database/dump.sql")
		if err != nil {
			panic(err)
		}

		_, err = db.Exec(string(file))
		if err != nil {
			panic(err)
		}
		// Create default user
		// TODO

		// Execute migrate UP
		result, err := IsEmpty(appPath + "/migrations")
		if err != nil {
			panic(err)
		}
		if !result {
			err = db.Close()
			if err != nil {
				panic(err)
			}
			migrateUpCmd.Run(cmd, nil)
		}

		fmt.Println("bootstrap done")
	},
}

func init() {
	rootCmd.AddCommand(bootstrapCmd)
	bootstrapCmd.AddCommand(bootstrapResetCmd)
}
