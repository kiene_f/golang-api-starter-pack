package models

import jwt "github.com/dgrijalva/jwt-go"

type JwtCustomClaims struct {
	//Login string `json:"login"`
	//Role  string `json:"role"`
	// Custom claims
	jwt.StandardClaims
}

type JWT struct {
	Token string `json:"token"`
}
