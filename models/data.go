package models

type Data struct {
	Model interface{} `json:"data"`
	Meta  Meta        `json:"meta,omitempty"`
}

type Meta struct {
	Count int `json:"count"`
}
