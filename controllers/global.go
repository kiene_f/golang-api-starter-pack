package controllers

import (
	"golang_api_starter_pack/database"
	"golang_api_starter_pack/models"
	"net/http"

	"github.com/labstack/echo"
)

func Get(c echo.Context, model interface{}) error {
	dt := &models.Data{Model: model}
	db := database.DBInstance(c)
	dbCount := database.DBInstance(c)

	if c.Param("id") != "" {
		if err := db.First(model, c.Param("id")).Count(&dt.Meta.Count).Error; err != nil {
			return NewError(c, http.StatusNotFound, err)
		}
	} else {
		db = db.Offset(c.FormValue("offset")).Limit(c.FormValue("limit")).Find(model)
		if err := db.Error; err != nil {
			return NewError(c, http.StatusNotFound, err)
		}

		dbCount = dbCount.Model(model)
		if err := dbCount.Count(&dt.Meta.Count).Error; err != nil {
			return NewError(c, http.StatusNotFound, err)
		}
	}

	return c.JSON(http.StatusOK, dt)
}

func Create(c echo.Context, model interface{}) error {
	dt := &models.Data{Model: model, Meta: models.Meta{Count: 1}}
	db := database.DBInstance(c)

	if err := c.Bind(model); err != nil {
		return NewError(c, http.StatusBadRequest, err)
	}

	if err := db.Create(model).Error; err != nil {
		return NewError(c, http.StatusBadRequest, err)
	}

	return c.JSON(http.StatusCreated, dt)
}

func Update(c echo.Context, model interface{}) error {
	dt := &models.Data{Model: model, Meta: models.Meta{Count: 1}}
	db := database.DBInstance(c)

	if c.Param("id") != "" {
		if err := db.First(model, c.Param("id")).Error; err != nil {
			return NewError(c, http.StatusNotFound, err)
		}
	}

	if err := c.Bind(model); err != nil {
		return NewError(c, http.StatusBadRequest, err)
	}

	if err := db.Save(model).Error; err != nil {
		return NewError(c, http.StatusBadRequest, err)
	}

	return c.JSON(http.StatusOK, dt)
}

func Delete(c echo.Context, model interface{}, typeName string) error {
	db := database.DBInstance(c)

	if err := db.First(model, c.Param("id")).Error; err != nil {
		return NewError(c, http.StatusNotFound, err)
	}

	if err := db.Delete(model).Error; err != nil {
		return NewError(c, http.StatusBadRequest, err)
	}

	return c.JSON(http.StatusNoContent, typeName+" with id#"+c.Param("id")+" deleted")
}
