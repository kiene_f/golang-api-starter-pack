## Golang API starter pack avec Echo et Gorm
##### Installation et lancement
- Installer Golang https://golang.org/doc/install
- Dépendances: `go get -u github.com/golang/dep/cmd/dep` puis `dep ensure`
- Build l'application `go build main.go`
- Initialiser l'application `main.exe init`
- Modifier le fichier de configuration `config/config.toml`
- `main.exe bootstrap`
- Lancer le server `main.exe server`

##### Commande disponible
- `main.exe --help` : Commande d'aide
- `main.exe server` : Lance l'API
- `main.exe bootstrap` : Créer la DB, install les triggers et créer l'utilisateur par défaut
- `main.exe init` : Copie le fichier config.example.toml en config.toml
- `main.exe migrate` : Utilitaire de migration (`main.exe migrate --help` liste les commandes disponibles)

##### Dependency management
- `dep ensure` : S'assure que le dossier vendor/ est à la bonne version de la configuration
- `dep ensure -add github.com/foo/bar` : Ajoute une dépendance
- `dep ensure -update` ou `dep ensure -update github.com/foo/bar` : Pour mettre à jour les ou la dépendance(s)
- `dep status` : Check le statut des dépendances
Plus d'informations sur : https://golang.github.io/dep/

### Libraries
##### Dependency management
- https://github.com/golang/dep
##### Framework
- https://github.com/labstack/echo
##### ORM
- https://github.com/jinzhu/gorm
##### Gorm Validator
- https://github.com/qor/validations
##### Generate Gorm model
- https://github.com/wantedly/pq2gorm
##### Marshals structs conditionally based on tags on the fields
- https://github.com/liip/sheriff
##### Escape for postgres
- https://github.com/tj/go-pg-escape
##### Generate documenation
- https://github.com/swaggo/swag
##### TOML Parsing
- https://github.com/BurntSushi/toml

# Example d'URL
/users?include=books&filter=books.id:gt:2&order=name,-books.id&offset=2&limit=10